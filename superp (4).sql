-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2021 at 06:59 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `superp`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('SUPER_ADMIN','ADMIN','TEACHER','STUDENT') NOT NULL DEFAULT 'SUPER_ADMIN',
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `role`, `image_value`, `image_dir`, `created_at`, `updated_at`) VALUES
('d301dde5-2db5-4a02-8b8b-0727fecc4b02', 'ADMIN', 'admin@gmail.com', '$2y$10$ximsC6u76dPxvNKPIqoA8ezGxBjkFn8/182BTuWD5DpnV0E0maA5C', 'SUPER_ADMIN', 'college_erp.png', 'webroot\\files\\Admins\\image_value\\', '2021-02-14 13:43:34', '2021-02-14 13:43:34'),
('e4364063-29d3-42b2-b7cc-e2b36356bb47', 'sanket', 'sanket@sanket.com', '$2y$10$l60g6Zs/ttcEdDCfWvsV9O46qWai50aDne51KCtN3sod6GMhUC8FG', 'SUPER_ADMIN', 'download (6).jpg', 'webroot\\files\\Admins\\image_value\\', '2021-02-27 12:05:13', '2021-02-27 12:07:32'),
('ebe5d287-dca2-4af5-9d90-adcc54191e38', 'HARSHIL new', 'harshil@walia.com', '$2y$10$0BVp25rZJrM0vuGiaTmqzeyyDsXcYf0473Ff1P88z2Z7BadIY/7Ia', 'SUPER_ADMIN', '6.PNG', 'webroot\\files\\Admins\\image_value\\', '2021-02-14 13:54:20', '2021-02-14 13:54:20');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE `assignment` (
  `id` char(36) NOT NULL,
  `department_id` char(36) NOT NULL,
  `semester_id` char(36) NOT NULL,
  `subject_id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `deadline` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `department_id`, `semester_id`, `subject_id`, `title`, `description`, `deadline`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('2e18db6e-ad66-464e-8a2a-d59968735bc6', 'a9f7049f-0a54-487c-a257-e3b19455f7ee', '2a293427-0707-466e-b289-9824a4ddb339', '7053b8e5-5cce-43ea-91dd-a05711e41302', 'ARVR ASSIGNMENT', 'Write 10 ARVR Technologies', '2021-03-21', 1, 0, '2021-03-19 16:18:36', '2021-03-19 16:18:36'),
('8061de86-dace-4188-95de-6abb1bd7e314', 'dc3b0c80-043d-41a5-be95-e6161455055f', '8bb6e16c-de8a-4998-8670-f66503aced23', '7053b8e5-5cce-43ea-91dd-a05711e41302', 'ARVR ASSIGNMENT', 'Write 10 Example', '2021-03-31', 1, 0, '2021-03-19 16:19:23', '2021-03-19 16:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` char(36) NOT NULL,
  `name` text NOT NULL,
  `author` text NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `rack_no` varchar(255) NOT NULL,
  `status` enum('AVAILABLE','NOT AVAILABLE') NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `subject_code`, `price`, `quantity`, `image_value`, `image_dir`, `rack_no`, `status`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('11bec9d6-e672-4f0b-a30c-4ed93b5e6f58', 'How to be a billonaire', 'SANKET', 'BN001', '0', '50', '', '', 'B-01', 'AVAILABLE', 1, 0, '2021-03-11 18:24:39', '2021-03-11 18:24:39'),
('2a8bee58-b0c9-46a5-bca8-00e6fa703b24', 'C++', 'HARSHIL', 'CN25', '100', '200', 'download (6).jpg', 'webroot\\files\\Books\\image_value\\', 'C-25', 'NOT AVAILABLE', 1, 0, '2021-02-27 15:13:27', '2021-02-28 09:46:23'),
('60eecab8-5d40-4aa9-b5aa-8e6f77ab66c7', 'PYTHON', 'SANKET', 'INI106', '100', '50000', '1783da03d8e21ca6ebc26b42a7450f4c.jpg', 'webroot\\files\\Books\\image_value\\', 'R-21', 'AVAILABLE', 1, 0, '2021-02-27 15:12:22', '2021-02-27 15:27:14'),
('d76813a3-609d-4837-973c-c73e598373e7', 'HTML', 'MIHIR', 'HT25', '500', '20', '', '', 'C-25', 'AVAILABLE', 1, 0, '2021-03-01 07:55:24', '2021-03-01 07:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` char(36) NOT NULL,
  `dept_name` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `dept_name`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('a9f7049f-0a54-487c-a257-e3b19455f7ee', 'BCA', 1, 0, '2021-03-02 05:14:25', '2021-03-02 05:14:25'),
('c53a5afe-74dd-4996-a305-84476a8e01bc', 'MBA', 1, 0, '2021-03-02 05:15:46', '2021-03-02 05:15:46'),
('dc3b0c80-043d-41a5-be95-e6161455055f', 'B.tech - CSE', 1, 0, '2021-03-02 05:14:48', '2021-03-02 05:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `from_date`, `to_date`, `details`, `image_value`, `image_dir`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('b9b3c61e-c3e0-40f4-ab7e-824de4e0dda1', 'Football Tournament', '2021-03-11', '2021-03-11', 'FOOTBALL TOURNAMENT AT OUR COLLEGE', 'download (1).jpg', 'webroot\\files\\Event\\image_value\\', 1, 0, '2021-03-09 16:55:21', '2021-03-09 16:58:30'),
('c3da694c-8d3f-43d8-bd5b-394b532a14fe', 'HACKATHON', '2021-03-09', '2021-03-09', 'HACKATHON COMPETION ', 'the-hunger-games-png-26156.png', 'webroot\\files\\Event\\image_value\\', 1, 0, '2021-03-09 16:51:45', '2021-03-09 19:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `id` char(36) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`id`, `exam_name`, `date`, `note`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('45931260-e641-41a6-9ce9-1652a3d654b4', 'Mid Semester ', '2021-03-09', 'Mid Semester Exam Starts', 1, 0, '2021-03-09 19:47:35', '2021-03-09 19:54:10'),
('bec07cd7-4b28-4a97-8e1e-e44250e2c1f1', 'End Semester', '2021-03-26', '', 1, 0, '2021-03-09 19:48:00', '2021-03-09 19:54:19');

-- --------------------------------------------------------

--
-- Table structure for table `examschedule`
--

CREATE TABLE `examschedule` (
  `id` char(36) NOT NULL,
  `department_id` char(36) NOT NULL,
  `subject_id` char(36) NOT NULL,
  `exam_id` char(36) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time_from` varchar(255) NOT NULL,
  `time_to` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `examschedule`
--

INSERT INTO `examschedule` (`id`, `department_id`, `subject_id`, `exam_id`, `date`, `time_from`, `time_to`, `room`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('6492bbfc-84cb-490a-a820-22e89ca3579c', 'dc3b0c80-043d-41a5-be95-e6161455055f', '702b6611-34f0-425e-9f37-ebbe9bd8abfc', '45931260-e641-41a6-9ce9-1652a3d654b4', '2021-03-14', '05:30', '15:30', '102', 1, 0, '2021-03-14 20:51:11', '2021-03-14 21:43:25'),
('c290a23f-68b1-4c5d-a728-069f7c9f31ce', 'c53a5afe-74dd-4996-a305-84476a8e01bc', 'c8bedd28-f0e2-4d38-8d96-81eac4361d6c', 'bec07cd7-4b28-4a97-8e1e-e44250e2c1f1', '2021-03-14', '15:30', '17:30', '101', 1, 0, '2021-03-14 20:49:25', '2021-03-14 20:49:25');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` char(36) NOT NULL,
  `grade_name` varchar(255) NOT NULL,
  `grade_point` varchar(255) NOT NULL,
  `mark_from` varchar(255) NOT NULL,
  `mark_upto` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `grade_name`, `grade_point`, `mark_from`, `mark_upto`, `note`, `image_value`, `image_dir`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('505cc73d-29a5-401b-bf50-ad0eb50d3127', 'A+', '5.00', '80', '100', 'Excellent', '8.png', 'webroot\\files\\Grade\\image_value\\', 1, 0, '2021-03-15 14:31:00', '2021-03-15 14:43:44'),
('e807ae44-1969-40e4-9c01-e377aa54293a', 'A', '4.00', '70', '79', 'WELL DONE', '', '', 1, 0, '2021-03-15 14:31:28', '2021-03-15 14:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE `holiday` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `from_date` varchar(255) NOT NULL,
  `to_date` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`id`, `title`, `from_date`, `to_date`, `details`, `image_value`, `image_dir`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('a163d0f0-083c-4f3b-bee4-bb1c9b2a5e58', 'MAHASHIVRATRI', '2021-03-09', '2021-03-09', '1 day holiday for MAHASHIVRATRI', 'holidays-png-5582.png', 'webroot\\files\\Holiday\\image_value\\', 1, 0, '2021-03-09 17:10:21', '2021-03-09 19:10:58');

-- --------------------------------------------------------

--
-- Table structure for table `issue`
--

CREATE TABLE `issue` (
  `id` char(36) NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `book_id` char(36) NOT NULL,
  `student_id` char(36) DEFAULT NULL,
  `teacher_id` char(36) DEFAULT NULL,
  `issue_date` datetime NOT NULL DEFAULT current_timestamp(),
  `due_date` varchar(255) NOT NULL,
  `return_date` varchar(255) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `issue`
--

INSERT INTO `issue` (`id`, `image_value`, `image_dir`, `book_id`, `student_id`, `teacher_id`, `issue_date`, `due_date`, `return_date`, `note`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('f2344fe7-3712-4d8e-8c8d-8925bd4b7534', '', '', '2a8bee58-b0c9-46a5-bca8-00e6fa703b24', '29126748-28cf-4308-9fbe-33dc5b5cc353', 'd0babdfa-b535-41be-b78d-82083e48c71a', '2021-02-28 14:21:52', '2021-02-17', '2021-03-25', '', 1, 0, '2021-02-28 14:21:52', '2021-03-07 02:15:23'),
('ff5d9785-a613-4c93-b63f-37ce699b1e8b', '', '', 'd76813a3-609d-4837-973c-c73e598373e7', '29126748-28cf-4308-9fbe-33dc5b5cc353', 'd0babdfa-b535-41be-b78d-82083e48c71a', '2021-03-07 02:19:09', '2021-03-17', '2021-03-25', 'sgs', 1, 0, '2021-03-07 02:19:09', '2021-03-07 02:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `notice` varchar(255) NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `title`, `date`, `notice`, `image_value`, `image_dir`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('ded6cfb6-5266-4a67-bd4f-1025a1f36c82', 'Annual Sports Day', '2021-03-10', 'In your school campus on 1-0...', 'download (1).png', 'webroot\\files\\Notice\\image_value\\', 1, 0, '2021-03-09 15:47:38', '2021-03-09 16:32:26'),
('e53533bd-ec12-4775-aa3a-db2f06bf475c', '	Programming Contest', '2021-03-12', 'On 20-11-2018 will held a programming cont...', 'download.jpg', 'webroot\\files\\Notice\\image_value\\', 1, 0, '2021-03-09 15:40:53', '2021-03-09 16:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` char(36) NOT NULL,
  `sem_name` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `sem_name`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('83744ecc-c179-4ac0-aded-a92e9b73f9d5', '1st Semester ', 1, 0, '2021-03-19 15:00:17', '2021-03-19 15:06:28'),
('2a293427-0707-466e-b289-9824a4ddb339', '2nd Semester', 1, 0, '2021-03-19 15:00:35', '2021-03-19 15:00:35'),
('a714cb6b-3267-4245-be15-692a3cdeb54a', '3rd Semester', 1, 0, '2021-03-19 15:00:45', '2021-03-19 15:00:45'),
('2e94b1a4-6c69-465b-9f7c-fb9cf99377e5', '4th Semester', 1, 0, '2021-03-19 15:00:59', '2021-03-19 15:00:59'),
('b7fbe243-e286-42dd-a910-c5099a063b0d', '5th Semester', 1, 0, '2021-03-19 15:01:09', '2021-03-19 15:01:09'),
('cca97473-bbfe-467b-895b-8a1fd4ed10c3', '6th Semester', 1, 0, '2021-03-19 15:01:19', '2021-03-19 15:01:19'),
('8bb6e16c-de8a-4998-8670-f66503aced23', '7th Semester', 1, 0, '2021-03-19 15:01:28', '2021-03-19 15:01:28'),
('daf1c0cd-a513-4b47-b2b5-7e9cee51ae5b', '8th Semester', 1, 0, '2021-03-19 15:01:46', '2021-03-19 15:01:46');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` char(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `address` text NOT NULL,
  `religion` varchar(255) NOT NULL,
  `caste` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `blood_group` enum('A+','A-','B+','B-','O+','O-','AB+','AB-') NOT NULL,
  `department_id` char(36) NOT NULL,
  `semester_id` char(36) NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roll_no` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `email`, `mobile`, `dob`, `gender`, `address`, `religion`, `caste`, `state`, `country`, `blood_group`, `department_id`, `semester_id`, `image_value`, `image_dir`, `username`, `password`, `roll_no`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('1cef4087-b8c5-4f28-9d4a-5c4d8b876b14', 'SANKET', 'PARMAR', 'sanket@sanket.com', '1234567890', '1999-12-04', 'MALE', 'LOREM', 'Hindu', 'General', 'GUJRAT', 'INDIA', 'AB+', 'dc3b0c80-043d-41a5-be95-e6161455055f', '8bb6e16c-de8a-4998-8670-f66503aced23', '', '', NULL, NULL, NULL, 1, 0, '2021-03-19 16:38:56', '2021-03-19 16:40:56'),
('4155d5bf-37ad-4a3d-8ad8-59b508db1888', 'HARSHIL', 'WALIA', 'test@test.com', '3216549870', '1999-02-15', 'MALE', 'Lorem', 'Hindu', 'General', 'GUJRAT', 'India', 'O+', 'c53a5afe-74dd-4996-a305-84476a8e01bc', '83744ecc-c179-4ac0-aded-a92e9b73f9d5', 'download (6).jpg', 'webroot\\files\\Students\\image_value\\', NULL, NULL, NULL, 1, 0, '2021-03-19 16:40:25', '2021-03-19 16:40:25'),
('a91366c7-5f51-4f17-833f-fadad746e0aa', 'KARTIK', 'NERKER', 'kartik@kartik.com', '1234567890', '1999-02-01', 'MALE', 'lorem', 'Hindu', 'General', 'GUJRAT', 'INDIA', 'A+', 'dc3b0c80-043d-41a5-be95-e6161455055f', 'cca97473-bbfe-467b-895b-8a1fd4ed10c3', '', '', NULL, NULL, NULL, 1, 0, '2021-03-20 17:37:20', '2021-03-20 17:37:20');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` char(36) NOT NULL,
  `department_id` char(36) NOT NULL,
  `semester_id` char(36) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `teacher_id` char(36) NOT NULL,
  `pass_mark` varchar(255) NOT NULL,
  `final_mark` varchar(255) NOT NULL,
  `subject_type` enum('MANDATORY','OPTIONAL') NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `department_id`, `semester_id`, `subject_name`, `subject_code`, `teacher_id`, `pass_mark`, `final_mark`, `subject_type`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('7053b8e5-5cce-43ea-91dd-a05711e41302', 'dc3b0c80-043d-41a5-be95-e6161455055f', '2a293427-0707-466e-b289-9824a4ddb339', 'AR VR', 'CN25', '280008aa-964c-4323-97c9-85e382287b7d', '10', '100', 'MANDATORY', 1, 0, '2021-03-19 15:21:17', '2021-03-19 16:05:11');

-- --------------------------------------------------------

--
-- Table structure for table `syllabus`
--

CREATE TABLE `syllabus` (
  `id` char(36) NOT NULL,
  `department_id` char(36) NOT NULL,
  `semester_id` char(36) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `syllabus`
--

INSERT INTO `syllabus` (`id`, `department_id`, `semester_id`, `title`, `description`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('83e7eb73-1923-47d7-8c0e-1248d1dac98a', 'dc3b0c80-043d-41a5-be95-e6161455055f', '2a293427-0707-466e-b289-9824a4ddb339', 'AR VR SYLLABUS', 'FIRST 5 CHAPTER FROM  ARVR MANUAL', 1, 0, '2021-03-19 16:03:18', '2021-03-19 16:04:35');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` char(36) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL DEFAULT 'MALE',
  `department_id` char(36) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `designation` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `caste` varchar(255) NOT NULL,
  `joining_date` varchar(255) NOT NULL,
  `leaving_date` varchar(255) DEFAULT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `blood_group` enum('A+','A-','B+','B-','O+','O-','AB+','AB-') NOT NULL,
  `image_value` varchar(255) NOT NULL,
  `image_dir` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `first_name`, `last_name`, `email`, `mobile`, `dob`, `gender`, `department_id`, `qualification`, `address`, `designation`, `religion`, `caste`, `joining_date`, `leaving_date`, `state`, `country`, `blood_group`, `image_value`, `image_dir`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('280008aa-964c-4323-97c9-85e382287b7d', 'BILL', 'GATES', 'bill@gates.com', '23213213', '2021-03-09', 'MALE', 'B.tech - CSE', '5', 'lorem', '', '', '', '', NULL, '', '', '', '', '', 1, 0, '2021-03-09 18:48:13', '2021-03-09 18:48:13'),
('aa311217-c461-43a6-a9db-71f1c1a0a357', 'Carry ', 'Minati', 'test@test.com', '1234567890', '1990-02-01', 'MALE', 'c53a5afe-74dd-4996-a305-84476a8e01bc', 'Phd', 'lorem', '', '', '', '', NULL, '', '', 'O+', '1.PNG', 'webroot\\files\\Teachers\\image_value\\', 1, 0, '2021-03-20 17:52:43', '2021-03-20 17:53:11'),
('d0babdfa-b535-41be-b78d-82083e48c71a', 'Arpan', 'Vyas', 'sanket@sanket.com', '2132168152', '2021-02-11', 'MALE', 'BCA', 'Phd', 'khbkh', '', '', '', '', NULL, '', '', '', 'architecture (1) (1).png', 'webroot\\files\\Teachers\\image_value\\', 1, 0, '2021-02-14 11:31:03', '2021-03-07 11:39:26');

-- --------------------------------------------------------

--
-- Table structure for table `transport`
--

CREATE TABLE `transport` (
  `id` char(36) NOT NULL,
  `route_name` text NOT NULL,
  `number_of_vehicle` varchar(255) NOT NULL,
  `route_fare` varchar(255) NOT NULL,
  `note` text DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transport`
--

INSERT INTO `transport` (`id`, `route_name`, `number_of_vehicle`, `route_fare`, `note`, `is_active`, `is_deleted`, `created_at`, `updated_at`) VALUES
('3e44e654-efc3-4a3c-91da-6553d055f262', 'VASNA ,amitnagar AMERICA', '2', '0', 'FREE OF COST SPONSER BY SANKET', 1, 0, '2021-03-06 12:08:22', '2021-03-08 12:16:21'),
('cd1f96da-4feb-4af7-9c9b-446704cfe6c8', 'AMERICA To CANADA', '1', '0', '', 1, 0, '2021-03-06 12:52:00', '2021-03-06 12:52:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment`
--
ALTER TABLE `assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examschedule`
--
ALTER TABLE `examschedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issue`
--
ALTER TABLE `issue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `syllabus`
--
ALTER TABLE `syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transport`
--
ALTER TABLE `transport`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
