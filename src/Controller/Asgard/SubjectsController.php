<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class SubjectsController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);


        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
        $department_id = $this->request->getQuery('department_id');


        if (isset($filter) && !empty($filter)) {
            $where[] = ['Subjects.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        if (isset($department_id) && !empty($department_id)) {
            $where[] = ['Subjects.department_id' => $department_id];
            $this->set('department_selected', $department_id);
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Subjects.name' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Departments', 'Teachers','Semester']
        ];
        $details = $this->Subjects->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        //Fetch Teachers
        $teachers = $this->Query->getDataByList('Teachers', ['Teachers.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('teachers', $teachers);

        //Fetch Semester
        $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id', 'sem_name']);
        $this->set('semester', $semester);

        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (isset($data['teacher_id'])) {
                $teacher_id = $data['teacher_id'];
                $tmp = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $teacher_id]);
                if (isset($tmp)) {
                    $data['teacher_name'] = $tmp['name'];
                }
            }

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }


            if ($this->Query->setData('Subjects', $data)) {
                $this->Flash->set('Subject ' . $data['subject_name'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Subjects', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Subjects');
    }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }

        if ($this->Query->removeData('Subjects', ['Subjects.id' => $id])) {
            $this->Flash->success('Subject ' . $data['subject_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        //Fetch Teacher
        $teachers = $this->Query->getDataByList('Teachers', ['Teachers.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('teachers', $teachers);

        //Fetch Semester
        $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id','sem_name']);
        $this->set('semester', $semester);

        $data = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (isset($data['teacher_id'])) {
                $teacher_id = $data['teacher_id'];
                $tmp = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $teacher_id]);
                if (isset($tmp)) {
                    $data['teacher_name'] = $tmp['name'];
                }
            }

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }
            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }


            $data['id'] = $id;
            if ($this->Query->setData('Subjects',  $data)) {
                $this->Flash->success('Subject ' . $data['subject_name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Subjects', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Subjects', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Subjects');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $id],[],['Departments','Teachers','Semester']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Subjects', 'action' => 'index'));
        }
    }
}
