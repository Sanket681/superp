<?php
namespace App\Controller\Asgard;
use App\Controller\Asgard\AppController;
// use App\Controller\Api\AppController;

class DashboardController extends AppController
{
    public $components = ['Query','Paginator'];
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow([]);
    }

    public function index() {
      $this->viewBuilder()->setLayout('backend_main');
    }

  

}