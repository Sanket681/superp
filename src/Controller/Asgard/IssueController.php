<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class IssueController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');

        if (isset($filter) && !empty($filter)) {
            $where[] = ['Issue.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Books.name' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Books', 'Students', 'Teachers']


        ];
        $details = $this->Issue->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    // ADD

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Book
        $books = $this->Query->getDataByList('Books', ['Books.is_active' => 1], ['id', 'name']);
        $this->set('books', $books);

        $students = $this->Query->getDataByList('Students', ['Students.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('students', $students);

        $teachers = $this->Query->getDataByList('Teachers', ['Teachers.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('teachers', $teachers);

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['book_id'])) {
                $book_id = $data['book_id'];
                $tmp = $this->Query->getAllDataById('Books', ['Books.id' => $book_id]);
                if (isset($tmp)) {
                    $data['book_name'] = $tmp['name'];
                }
            }
            if (isset($data['student_id'])) {
                $student_id = $data['student_id'];
                $tmp = $this->Query->getAllDataById('Students', ['Students.id' => $student_id]);
                if (isset($tmp)) {
                    $data['student_name'] = $tmp['name'];
                }
            }

            if (isset($data['teacher_id'])) {
                $teacher_id = $data['teacher_id'];
                $tmp = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $teacher_id]);
                if (isset($tmp)) {
                    $data['teacher_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Issue', $data)) {
                $this->Flash->set('Issue ' . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Issue', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Issue');
    }

    // DELETE

    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Issue', ['Issue.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Issue not found.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }

        if ($this->Query->removeData('Issue', ['Issue.id' => $id])) {
            $this->Flash->success('Issue ' .  ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Book
        $books = $this->Query->getDataByList('Books', ['Books.is_active' => 1], ['id', 'name']);
        $this->set('books', $books);

        $students = $this->Query->getDataByList('Students', ['Students.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('students', $students);

        $teachers = $this->Query->getDataByList('Teachers', ['Teachers.is_active' => 1], ['id', 'first_name', 'last_name']);
        $this->set('teachers', $teachers);


        $data = $this->Query->getAllDataById('Issue', ['Issue.id' => $id],[],['Students','Teachers','Books']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Issue not found.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Issue',  $data)) {
                $this->Flash->success('Issue ' .  ' has been edited.');
                return $this->redirect(array('controller' => 'Issue', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Issue', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Issue');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Issue', ['Issue.id' => $id],[],['Books','Students','Teachers']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Issue not found.');
            return $this->redirect(array('controller' => 'Issue', 'action' => 'index'));
        }
    }
}
