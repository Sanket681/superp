<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class SyllabusController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);


        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
        $department_id = $this->request->getQuery('department_id');


        if (isset($filter) && !empty($filter)) {
            $where[] = ['Syllabus.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        if (isset($department_id) && !empty($department_id)) {
            $where[] = ['Syllabus.department_id' => $department_id];
            $this->set('department_selected', $department_id);
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Syllabus.title' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Departments', 'Semester']
        ];
        $details = $this->Syllabus->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);


        //Fetch Semester
        $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id', 'sem_name']);
        $this->set('semester', $semester);



        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Syllabus', $data)) {
                $this->Flash->set('Syllabus ' . $data['title'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Syllabus', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Syllabus');
    }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Syllabus', ['Syllabus.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Syllabus not found.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }

        if ($this->Query->removeData('Syllabus', ['Syllabus.id' => $id])) {
            $this->Flash->success('Syllabus ' . $data['title'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

         //Fetch Semester
         $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id','sem_name']);
         $this->set('semester', $semester);

        $data = $this->Query->getAllDataById('Syllabus', ['Syllabus.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Syllabus not found.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }


            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Syllabus',  $data)) {
                $this->Flash->success('Syllabus ' . $data['title'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Syllabus', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Syllabus');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Syllabus', ['Syllabus.id' => $id], [], ['Departments','Semester']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Syllabus not found.');
            return $this->redirect(array('controller' => 'Syllabus', 'action' => 'index'));
        }
    }
}
