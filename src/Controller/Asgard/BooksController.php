<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class BooksController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');

        if (isset($filter) && !empty($filter)) {
            $where[] = ['Books.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Books.name' => 'asc'
            ],
            'conditions' => $where
        ];
        $details = $this->Books->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    // ADD
    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Books', $data)) {
                $this->Flash->set('Book ' . $data['name'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Books', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Books');
    }

    //   EDIT
    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Books', ['Books.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Book not found.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Books',  $data)) {
                $this->Flash->success('Book ' . $data['name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Books', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Books', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Books');
    }

    // VIEW

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Books', ['Books.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Book not found.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }
    }

    // DELETE

    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Books', ['Books.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Book not found.');
            return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
        }

        if ($this->Query->removeData('Books', ['Books.id' => $id])) {
            $this->Flash->success('Book ' . $data['first_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Books', 'action' => 'index'));
    }

}
