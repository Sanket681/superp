<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class ExamscheduleController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        // Fetch EXAM NAME
        $exam = $this->Query->getDataByList('Exam', ['Exam.is_active' => 1], ['id', 'exam_name']);
        $this->set('exam', $exam);

        // Fetch SUBJECT
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
        $department_id = $this->request->getQuery('department_id');
        $exam_id = $this->request->getQuery('exam_id');
        $subject_id = $this->request->getQuery('subject_id');


        if (isset($filter) && !empty($filter)) {
            $where[] = ['Examschedule.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        if (isset($department_id) && !empty($department_id)) {
            $where[] = ['Examschedule.department_id' => $department_id];
            $this->set('department_selected', $department_id);
        }

        if (isset($exam_id) && !empty($exam_id)) {
            $where[] = ['Examschedule.exam_id' => $exam_id];
            $this->set('exam_selected', $exam_id);
        }

        if (isset($subject_id) && !empty($subject_id)) {
            $where[] = ['subjectschedule.subject_id' => $subject_id];
            $this->set('subject_selected', $subject_id);
        }


        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Examschedule.name' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Departments','Exam','Subjects']
        ];
        $details = $this->Examschedule->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);


        // Fetch EXAM NAME
        $exam = $this->Query->getDataByList('Exam', ['Exam.is_active' => 1], ['id', 'exam_name']);
        $this->set('exam', $exam);

        // Fetch SUBJECT
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (isset($data['exam_id'])) {
                $exam_id = $data['exam_id'];
                $tmp = $this->Query->getAllDataById('Exam', ['Exam.id' => $exam_id]);
                if (isset($tmp)) {
                    $data['exam_name'] = $tmp['name'];
                }
            }

            if (isset($data['subject_id'])) {
                $subject_id = $data['subject_id'];
                $tmp = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $subject_id]);
                if (isset($tmp)) {
                    $data['subject_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Examschedule', $data)) {
                $this->Flash->set('Examschedule ' . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Examschedule', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Examschedule');
    }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Examschedule', ['Examschedule.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Examschedule not found.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }

        if ($this->Query->removeData('Examschedule', ['Examschedule.id' => $id])) {
            $this->Flash->success('Examschedule ' . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');

        
        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        // Fetch EXAM NAME
        $exam = $this->Query->getDataByList('Exam', ['Exam.is_active' => 1], ['id', 'exam_name']);
        $this->set('exam', $exam);

        // Fetch SUBJECT
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);


        $data = $this->Query->getAllDataById('Examschedule', ['Examschedule.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Examschedule not found.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Examschedule',  $data)) {
                $this->Flash->success('Examschedule ' .  ' has been edited.');
                return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Examschedule', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Examschedule');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Examschedule', ['Examschedule.id' => $id],[],['Departments','Subjects','Exam']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Examschedule not found.');
            return $this->redirect(array('controller' => 'Examschedule', 'action' => 'index'));
        }
    }
}
