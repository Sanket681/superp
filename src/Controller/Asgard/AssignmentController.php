<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class AssignmentController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        // Fetch Subject
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);


        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
        $department_id = $this->request->getQuery('department_id');
        $subject_id = $this->request->getQuery('subject_id');


        if (isset($filter) && !empty($filter)) {
            $where[] = ['Assignment.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        if (isset($department_id) && !empty($department_id)) {
            $where[] = ['Assignment.department_id' => $department_id];
            $this->set('department_selected', $department_id);
        }

        if (isset($subject_id) && !empty($subject_id)) {
            $where[] = ['Assignment.subject_id' => $subject_id];
            $this->set('subject_selected', $subject_id);
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Assignment.title' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Departments', 'Subjects', 'Semester']
        ];
        $details = $this->Assignment->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        // Fetch Subject
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);

        //Fetch Semester
        $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id', 'sem_name']);
        $this->set('semester', $semester);



        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (isset($data['subject_id'])) {
                $subject_id = $data['subject_id'];
                $tmp = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $subject_id]);
                if (isset($tmp)) {
                    $data['subject_name'] = $tmp['name'];
                }
            }

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }


            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Assignment', $data)) {
                $this->Flash->set('Assignment ' . $data['title'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Assignment', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Assignment');
    }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Assignment', ['Assignment.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }

        if ($this->Query->removeData('Assignment', ['Assignment.id' => $id])) {
            $this->Flash->success('Assignment ' . $data['title'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        // Fetch Subject
        $subjects = $this->Query->getDataByList('Subjects', ['Subjects.is_active' => 1], ['id', 'subject_name']);
        $this->set('subjects', $subjects);

         //Fetch Semester
         $semester = $this->Query->getDataByList('Semester', ['Semester.is_active' => 1], ['id','sem_name']);
         $this->set('semester', $semester);

        $data = $this->Query->getAllDataById('Assignment', ['Assignment.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (isset($data['subject_id'])) {
                $subject_id = $data['subject_id'];
                $tmp = $this->Query->getAllDataById('Subjects', ['Subjects.id' => $subject_id]);
                if (isset($tmp)) {
                    $data['subject_name'] = $tmp['name'];
                }
            }

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (isset($data['semester_id'])) {
                $semester_id = $data['semester_id'];
                $tmp = $this->Query->getAllDataById('Semester', ['Semester.id' => $semester_id]);
                if (isset($tmp)) {
                    $data['semester_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Assignment',  $data)) {
                $this->Flash->success('Assignment ' . $data['title'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Assignment', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Assignment', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Assignment');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Assignment', ['Assignment.id' => $id], [], ['Departments', 'Subjects','Semester']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Subject not found.');
            return $this->redirect(array('controller' => 'Assignment', 'action' => 'index'));
        }
    }
}
