<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class SemesterController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
    
        if (isset($filter) && !empty($filter)) {
          $where[] = ['Semester.' .$filter. ' LIKE' => '%'.$search.'%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Semester.sem_name' => 'asc'
            ],
            'conditions' => $where
        ];
        $details = $this->Semester->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add() {
        $this->viewBuilder()->setLayout('backend_main');
    
        if ($this->request->is('post')) {
          $data = $this->request->getData();
                    

          if ($this->Query->setData('Semester', $data)) {
            $this->Flash->set('Semester '.$data['sem_name']. ' has been added.', [
              'element' => 'success'
            ]);
          
            return $this -> redirect(array('controller' => 'Semester', 'action' => 'index'));
          } else {
            $this->Flash->set('Oops! Something went wrong. Please try again later.', [
              'element' => 'error'
            ]);
            return $this -> redirect(array('controller' => 'Semester', 'action' => 'add'));
          }
        }
        $this->set('page_title', 'Add Semester');
      }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Semester', ['Semester.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Semester not found.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }

        if ($this->Query->removeData('Semester', ['Semester.id' => $id])) {
            $this->Flash->success('Semester ' . $data['sem_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Semester', ['Semester.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Semester not found.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();
          

            $data['id'] = $id;
            if ($this->Query->setData('Semester',  $data)) {
                $this->Flash->success('Semester ' . $data['sem_name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Semester', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Semester', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Semester');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Semester', ['Semester.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Semester not found.');
            return $this->redirect(array('controller' => 'Semester', 'action' => 'index'));
        }
    }
}
