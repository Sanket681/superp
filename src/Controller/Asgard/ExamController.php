<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class ExamController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');

        if (isset($filter) && !empty($filter)) {
            $where[] = ['Exam.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Exam.name' => 'asc'
            ],
            'conditions' => $where
        ];
        $details = $this->Exam->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }


    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if ($this->Query->setData('Exam', $data)) {
                $this->Flash->set('Exam ' . $data['exam_name'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Exam', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Exam');
    }

    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Exam', ['Exam.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Exam not found.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }

        if ($this->Query->removeData('Exam', ['Exam.id' => $id])) {
            $this->Flash->success('Exam ' . $data['exam_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Exam', ['Exam.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Exam not found.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $data['id'] = $id;
            if ($this->Query->setData('Exam',  $data)) {
                $this->Flash->success('Exam ' . $data['exam_name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Exam', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Exam', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Exam');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Exam', ['Exam.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Exam not found.');
            return $this->redirect(array('controller' => 'Exam', 'action' => 'index'));
        }
    }
}
