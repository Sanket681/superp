<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class TeachersController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
        $department_id = $this->request->getQuery('department_id');


        if (isset($filter) && !empty($filter)) {
            $where[] = ['Teachers.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        if (isset($department_id) && !empty($department_id)) {
            $where[] = ['Students.department_id' => $department_id];
            $this->set('department_selected', $department_id);
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Teachers.name' => 'asc'
            ],
            'conditions' => $where,
            'contain' => ['Departments']
        ];
        $details = $this->Teachers->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        if ($this->request->is('post')) {
            $data = $this->request->getData();


            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            if ($this->Query->setData('Teachers', $data)) {
                $this->Flash->set('Teacher ' . $data['first_name'] . $data['last_name'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Teachers', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Teachers');
    }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Teacher not found.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }

        if ($this->Query->removeData('Teachers', ['Teachers.id' => $id])) {
            $this->Flash->success('Teacher ' . $data['first_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');


        // Fetch Department
        $departments = $this->Query->getDataByList('Departments', ['Departments.is_active' => 1], ['id', 'dept_name']);
        $this->set('departments', $departments);

        $data = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Teacher not found.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (isset($data['department_id'])) {
                $department_id = $data['department_id'];
                $tmp = $this->Query->getAllDataById('Departments', ['Departments.id' => $department_id]);
                if (isset($tmp)) {
                    $data['department_name'] = $tmp['name'];
                }
            }

            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Teachers',  $data)) {
                $this->Flash->success('Teacher ' . $data['first_name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Teachers', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Teachers', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Teachers');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Teachers', ['Teachers.id' => $id], [], ['Departments']);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Teacher not found.');
            return $this->redirect(array('controller' => 'Teachers', 'action' => 'index'));
        }
    }
}
