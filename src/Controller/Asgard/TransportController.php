<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class TransportController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');

        if (isset($filter) && !empty($filter)) {
            $where[] = ['Transport.' . $filter . ' LIKE' => '%' . $search . '%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Transport.name' => 'asc'
            ],
            'conditions' => $where
        ];
        $details = $this->Transport->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }


    public function add()
    {
        $this->viewBuilder()->setLayout('backend_main');

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if ($this->Query->setData('Transport', $data)) {
                $this->Flash->set('Transport ' . $data['route_name'] . ' has been added.', [
                    'element' => 'success'
                ]);

                return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
            } else {
                $this->Flash->set('Oops! Something went wrong. Please try again later.', [
                    'element' => 'error'
                ]);
                return $this->redirect(array('controller' => 'Transport', 'action' => 'add'));
            }
        }
        $this->set('page_title', 'Add Transport');
    }

    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Transport', ['Transport.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Transport not found.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }

        if ($this->Query->removeData('Transport', ['Transport.id' => $id])) {
            $this->Flash->success('Transport ' . $data['route_name'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Transport', ['Transport.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Transport not found.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            $data['id'] = $id;
            if ($this->Query->setData('Transport',  $data)) {
                $this->Flash->success('Transport ' . $data['route_name'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Transport', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Transport', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Transport');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Transport', ['Transport.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Transport not found.');
            return $this->redirect(array('controller' => 'Transport', 'action' => 'index'));
        }
    }
}
