<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;
// use App\Controller\Api\AppController;

class AdminsController extends AppController
{
  public $components = ['Query', 'Paginator', 'Special'];

  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow(['login', 'forgot_password', 'reset_password']);
  }

  public function forgot_password()
  {
    $this->viewBuilder()->setLayout('backend_login'); //base_layout
  }

  public function reset_password()
  {
    $this->viewBuilder()->setLayout('backend_login'); //base_layout
  }

  public function logout()
  {
    return $this->redirect($this->Auth->logout());
  }

  /**
   * Admin Login Method
   * 
   * @author Shoaib Shaikh <soeb.shaikh@ascendtis.com>
   * @return void
   */

  public function login()
  {
    $this->viewBuilder()->setLayout("backend_login");

    if (!empty($this->Auth->user())) {
      return $this->redirect($this->Auth->redirectUrl());
    }
    if ($this->request->is('post')) {
      $data = $this->request->getData();
      $admin = $this->Auth->identify();
      try {
        if ($admin) {
          $this->Auth->setUser($admin);

          if (isset($data['remember_me']) && !empty($data['remember_me'])) {
            $this->RememberMe->setUserData($admin);
          }

          $this->Flash->success('LoggedIn Successfully!');
          if ($admin['role'] == 'SUPER_ADMIN') {
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
          } else if ($admin['role'] == 'ADMIN') {
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
          } elseif ($admin['role'] == 'Admin') {
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
          } elseif ($admin['role'] == 'STUDENT') {
            return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
          }
          return $this->redirect($this->Auth->redirectUrl());
        } else {
          $this->Flash->error('Your username or password is incorrect.');
        }
        //code...
      } catch (\Throwable $th) {
        throw $th;
      }
    }
  }

  private function _accessDenied()
  {
    if ($this->Auth->user('role') != 'SUPER_ADMIN') {
      $this->Flash->error('Oops! You don\'t have permission to access that page.');
      $this->Auth->logout();
      return $this->redirect(array('controller' => 'admins', 'action' => 'login'));
    }
  }

  public function index()
  {
    $this->viewBuilder()->setLayout('backend_main');
    $this->_accessDenied();

    $where = [];
    $filter = $this->request->getQuery('filter');
    $search = $this->request->getQuery('search');

    if (isset($filter) && !empty($filter)) {
      $where[] = ['Admins.' . $filter . ' LIKE' => '%' . $search . '%'];
    }

    $this->paginate = [         //before it was `public` outside of the function
      'limit' => 10,
      'order' => [
        'Admins.name' => 'asc'
      ],
      'conditions' => $where,
    ];
    $details = $this->Admins->find('all');
    $this->set('data', $this->paginate($details));
    $this->set('filter', $filter);
    $this->set('search', $search);
  }

  public function add()
  {
    $this->viewBuilder()->setLayout('backend_main');
    $this->_accessDenied();

    if ($this->request->is('post')) {
      $data = $this->request->getData();
      // pr($data); //print Array
      // die;

      if ($data['password'] != $data['rpassword']) {
        $this->Flash->set('Your password and retype password is not matched.', [
          'element' => 'error'
        ]);
        return $this->redirect(array('controller' => 'admins', 'action' => 'add'));
      }

      $checkUsername = $this->Admins->find()->where(['Admins.username' => $data['username']]);
      if ($checkUsername->count() > 0) {
        $this->Flash->set('Oops! Username is already exists.', [
          'element' => 'error'
        ]);
        return $this->redirect(array('controller' => 'admins', 'action' => 'add'));
      }

      unset($data['rpassword']);

      //Add Admin
      if ($this->Query->setData('Admins', $data)) {
        $this->Flash->set('Admin ' . $data['name'] . ' has been added.', [
          'element' => 'success'
        ]);
        return $this->redirect(array('controller' => 'admins', 'action' => 'index'));
      } else {
        $this->Flash->set('Oops! Something went wrong. Please try again later.', [
          'element' => 'error'
        ]);
        return $this->redirect(array('controller' => 'admins', 'action' => 'add'));
      }
    }
    $this->set('page_title', 'Add Admin');
  }

  /**
   * Edit Admin
   * 
   * @param 
   */
  // public function edit($id = null) {
  //   $this->_accessDenied();

  //   if ($id === null) {
  //     $this->Flash->error('Invalid Arguments.');
  //     return $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
  //   }
  //   $this->viewBuilder()->setLayout('backend_main');
  //   $data = $this->Query->getAllDataById('Admins', [ 'Admins.id' => $id ]);
  //   if (isset($data['id'])) {
  //     $this->set('data', $data);
  //   } else {
  //     $this->Flash->error('Oops! Admin not found.');
  //     return $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
  //   }

  //   if ($this->request->is('post')) {
  //     $data = $this->request->getData();
  //     if(trim($data['password']) != trim($data['rpassword'])) {
  //       $this->Flash->error('Oops! Both password must be same or just leave it blank.');
  //       return $this -> redirect(array('controller' => 'admins', 'action' => 'edit', $id));
  //     }
  //     if (empty($data['image_value']['tmp_name'])) {
  //       unset($data['image_value']);
  //     }
  //     $data['id'] = $id;
  //     unset($data['rpassword']);
  //     if(trim($data['password']) < 2  || trim($data['password']) === '') {
  //       unset($data['password']);
  //     }

  //     // pr($data);die();

  //     if ($this->Query->setData('Admins',  $data)) {
  //       $this->Flash->success('Admin `'.$data['name'].'` has been edited.');
  //       return $this -> redirect(array('controller' => 'admins', 'action' => 'index'));
  //     } else {
  //       $this->Flash->error('Oops! Something went wrong. Please try again later.');
  //       return $this -> redirect(array('controller' => 'admins', 'action' => 'edit', $id));  
  //     }

  //   }

  //   $this->set('page_title', 'Edit Admin');
  // }

  public function edit($id = null)
  {
    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this->redirect(array('controller' => 'Admins', 'action' => 'index'));
    }
    $this->viewBuilder()->setLayout('backend_main');
    $this->_accessDenied();
    $data = $this->Query->getAllDataById('Admins', ['Admins.id' => $id]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Admin not found.');
      return $this->redirect(array('controller' => 'Admins', 'action' => 'index'));
    }

    if ($this->request->is('post')) {
      $data = $this->request->getData();
      if (trim($data['password']) != trim($data['rpassword'])) {
        $this->Flash->error('Oops! Both password must be same or just leave it blank.');
        return $this->redirect(array('controller' => 'admins', 'action' => 'edit', $id));
      }
      if (empty($data['image_value']['tmp_name'])) {
        unset($data['image_value']);
      }
      $data['id'] = $id;
      unset($data['rpassword']);
      if (trim($data['password']) < 2  || trim($data['password']) === '') {
        unset($data['password']);
      }

      if ($this->Query->setData('Admins',  $data)) {
        $this->Flash->success('Admin `' . $data['username'] . '` has been edited.');
        return $this->redirect(array('controller' => 'Admins', 'action' => 'index', $id));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this->redirect(array('controller' => 'Admins', 'action' => 'edit', $id));
      }
    }

    $this->set('page_title', 'Edit Admin');
  }


  public function delete($id = null)
  {
    $this->_accessDenied();

    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this->redirect(array('controller' => 'admins', 'action' => 'index'));
    }
    $data = $this->Query->getAllDataById('Admins', ['Admins.id' => $id]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Admin not found.');
      return $this->redirect(array('controller' => 'admins', 'action' => 'index'));
    }

    if ($this->Query->removeData('Admins', ['Admins.id' => $id])) {
      $this->Flash->success('The Admin ' . $data['name'] . ' has been deleted.');
    } else {
      $this->Flash->error('Oops! Something went wrong. Please try again later.');
    }
    return $this->redirect(array('controller' => 'admins', 'action' => 'index'));
  }

  public function change_password()
  {
    $this->viewBuilder()->setLayout('backend_main');

    $user = $this->Auth->user();
    if ($this->request->is('post')) {
      $data = $this->request->getData();

      if (trim($data['password']) != trim($data['rpassword'])) {
        $this->Flash->error('Oops! Both password must be same.');
        return $this->redirect(array('controller' => 'admins', 'action' => 'change_password'));
      }

      unset($data['rpassword']);
      $data['id'] = $user['id'];
      if ($this->Query->setData('Admins',  $data)) {
        $this->Flash->success('Password has been changed successfully.');
        return $this->redirect(array('controller' => 'admins', 'action' => 'index'));
      } else {
        $this->Flash->error('Oops! Something went wrong. Please try again later.');
        return $this->redirect(array('controller' => 'admins', 'action' => 'change_password'));
      }
    }
    $this->set('page_title', 'Change Password');
  }

  public function view($id = null)
  {
    if ($id === null) {
      $this->Flash->error('Invalid Arguments.');
      return $this->redirect(array('controller' => 'Admins', 'action' => 'index'));
    }
    $this->viewBuilder()->setLayout('backend_main');
    $data = $this->Query->getAllDataById('Admins', ['Admins.id' => $id]);
    if (isset($data['id'])) {
      $this->set('data', $data);
    } else {
      $this->Flash->error('Oops! Category not found.');
      return $this->redirect(array('controller' => 'Admins', 'action' => 'index'));
    }
  }

  public function profile()
  {
    $this->viewBuilder()->setLayout('backend_main');
  }
}
