<?php

namespace App\Controller\Asgard;

use App\Controller\Asgard\AppController;

class HolidayController extends AppController
{
    public $components = ['Query', 'Paginator', 'Special'];
    public function initialize()
    {
        parent::initialize();
    }
    //index
    public function index()
    {
        $this->viewBuilder()->setLayout('backend_main');

        $where = [];

        $filter = $this->request->getQuery('filter');
        $search = $this->request->getQuery('search');
    
        if (isset($filter) && !empty($filter)) {
          $where[] = ['Holiday.' .$filter. ' LIKE' => '%'.$search.'%'];
        }

        $this->paginate = [  //before it was `public` outside of the function
            'limit' => 10,
            'order' => [
                'Holiday.title' => 'asc'
            ],
            'conditions' => $where
        ];
        $details = $this->Holiday->find('all');
        $this->set('data', $this->paginate($details));
        $this->set('filter', $filter);
        $this->set('search', $search);
    }

    public function add() {
        $this->viewBuilder()->setLayout('backend_main');
    
        if ($this->request->is('post')) {
          $data = $this->request->getData();
          
          if (empty($data['image_value']['tmp_name'])) {
            unset($data['image_value']);
            }   

          if ($this->Query->setData('Holiday', $data)) {
            $this->Flash->set('Holiday '.$data['title']. ' has been added.', [
              'element' => 'success'
            ]);
          
            return $this -> redirect(array('controller' => 'Holiday', 'action' => 'index'));
          } else {
            $this->Flash->set('Oops! Something went wrong. Please try again later.', [
              'element' => 'error'
            ]);
            return $this -> redirect(array('controller' => 'Holiday', 'action' => 'add'));
          }
        }
        $this->set('page_title', 'Add Holiday');
      }
    public function delete($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }
        $data = $this->Query->getAllDataById('Holiday', ['Holiday.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Holiday not found.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }

        if ($this->Query->removeData('Holiday', ['Holiday.id' => $id])) {
            $this->Flash->success('Holiday ' . $data['title'] . ' has been deleted.');
        } else {
            $this->Flash->error('Oops! Something went wrong. Please try again later.');
        }
        return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Holiday', ['Holiday.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Holiday not found.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            
            if (empty($data['image_value']['tmp_name'])) {
                unset($data['image_value']);
            }

            $data['id'] = $id;
            if ($this->Query->setData('Holiday',  $data)) {
                $this->Flash->success('Holiday ' . $data['title'] .  ' has been edited.');
                return $this->redirect(array('controller' => 'Holiday', 'action' => 'index', $id));
            } else {
                $this->Flash->error('Oops! Something went wrong. Please try again later.');
                return $this->redirect(array('controller' => 'Holiday', 'action' => 'edit', $id));
            }
        }

        $this->set('page_title', 'Edit Holiday');
    }

    public function view($id = null)
    {
        if ($id === null) {
            $this->Flash->error('Invalid Arguments.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }
        $this->viewBuilder()->setLayout('backend_main');
        $data = $this->Query->getAllDataById('Holiday', ['Holiday.id' => $id]);
        if (isset($data['id'])) {
            $this->set('data', $data);
        } else {
            $this->Flash->error('Oops! Holiday not found.');
            return $this->redirect(array('controller' => 'Holiday', 'action' => 'index'));
        }
    }
}
