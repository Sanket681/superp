<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * PHP version 7
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @category  Component
 * @package   Special
 * @author    Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @copyright 2020 Copyright (c) Ascendtis IT Solutions.
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * @version   SVN: $Id$
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 */
namespace App\Controller\Component;
use Cake\Controller\Component;

/**
 * Setting Component
 *
 * @category Component
 * @package  Setting
 * @author   Mohammed Sufyan Shaikh <sufyan@ascendtis.com>
 * @license  http://www.opensource.org/licenses/mit-license.php MIT License
 * @link     https://www.ascendtis.com/
 */

class SettingComponent extends Component
{
  public $components = ['Query'];

  /**
   * Get App Settings
   * 
   * @return array
   */
  public function getAppSettings($baseImageUrl = null) {
    $data = $this->Query->getAllData('Settings', ['Settings.is_active' => 1], ['Settings.priority ASC'], false, ['SettingValues', 'SettingValues.Media']);
    $settings = [];
    
    $settings = [];
    foreach ($data as $key => $value) {
      $settings[$value['alias']] = [];
      foreach ($value['setting_values'] as $sKey => $sVal) {
        if ($sVal['input_type'] == 'PICTURE') {
          if (isset($sVal['media']['id'])) {
            $sVal['media']['url'] = $baseImageUrl.'Media/value/'.$sVal['media']['value'];
          }
          $settings[$value['alias']][$sVal['alias']] = $sVal['media'];

        } else if ($sVal['input_type'] == 'TOGGLE') {
          $settings[$value['alias']][$sVal['alias']] = (bool) $sVal['value'];
        } else {
          $settings[$value['alias']][$sVal['alias']] = $sVal['value'];
        }
      }
    }

    return $settings;
  }
}
?>