<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;


class ServicesController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->Auth->allow(['getProducts']);
  }

  public function getProducts()
  {
    $this->loadComponent('Query');
    $this->loadComponent('Special');
    $this->loadModel('Products');
    $query = $this->request->getQuery('query');

    $data = $this->Query->getAllData('Products', 
    ['Products.is_active' => 1,
    'Products.name LIKE' => '%'.$query.'%',
    ],
    ['Products.name' => 'ASC'],
    false,
    );
      
    $this->set([
      'success' => true,
      'data' => $data,
      '_serialize' => ['success','data']
    ]);
  }

}
?>
