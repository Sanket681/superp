<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Holiday</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Holiday", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Holiday</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>


                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" value="<?= $data['title'] ?>" placeholder="Please Enter Title" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="from_date">From Date</label>
                                    <input type="date" name="from_date" value="<?= $data['from_date']?>" placeholder="Please Enter From Date" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="to_date">To Date</label>
                                    <input type="date" name="to_date" value="<?= $data['from_date']?>" placeholder="Please Enter From Date" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="details">Details</label>
                            <textarea name="details" placeholder="Please Enter Details" class="form-control" required><?= $data['details'] ?></textarea>

                        </div>

                        <div class="file-upload">
                            <button class="btn btn-accent btn-block" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Edit Image</button>
                            <div class="image-upload-wrap">
                                <input class="file-upload-input" type='file' name="image_value" onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h5>Drag and drop a file or select add Image</h5>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img id="product-primary-image" class="file-upload-image" src="<?= $this->request->getAttribute('webroot') . $data['image_dir'] . $data['image_value'] ?>" alt="your image" data="<?= $data['image_value'] ?>" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Holiday</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>