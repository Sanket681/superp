<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">View Holiday</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Holiday", "action" => "index"]); ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Holiday</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section>
    <div class="row">
        <div class="col-lg-8 mx-auto mt-4">
            <div class="card card-small edit-user-details mb-4">
                <div class="card-header p-0">
                    <div class="edit-user-details__bg">
                        <img src=" <?php if (!empty($data['image_value'])) : ?><?= $this->request->getAttribute('webroot') . $data['image_dir'] . $data['image_value'] ?><?php else : ?><?= $this->request->getAttribute('webroot') . 'img/backend/' ?>background.jpg <?php endif; ?>" height="115" alt="Holiday Details Background Image">
                        <label class="edit-user-details__change-background">
                            <i class="material-icons mr-1">receipt_long</i> Holiday Details
                        </label>
                    </div>
                </div>
                <div class="card-body p-0">
                    <form action="#" class="py-4">
                        <div class="form-row mx-4">
                            <div class="col-lg-12">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Title</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['title'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>From Date</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['from_date'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>To Date</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['to_date'] ?></p>
                                    </div>
                                   
                                   
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Is Active</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['is_active'] ? 'Yes' : 'No' ?></p>
                                    </div>
                                    <div class="form-group col-md-12 text-justify">
                                        <h6 class="form-text m-0"><b>Details</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['details'] ?></p>
                                    </div>
                                </div>
                               
                            </div>
                           

                            <div class="form-group mb-0">
                                <a href="<?= $this->Url->build(["controller" => "Holiday", "action" => "edit", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-white active-light btn-sm sticky-action-btn-edit" data-toggle="tooltip" title="Edit Holiday">
                                    <i class="material-icons"></i> Edit
                                </a>
                                <a href="<?= $this->Url->build(["controller" => "Holiday", "action" => "delete", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-sm btn-danger sticky-action-btn-delete" data-toggle="tooltip" title="Delete Holiday">
                                    <i class="material-icons white"></i> Delete
                                </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>