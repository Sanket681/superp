    <!-- Page Header -->
    <section class="content-header">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">SUPERP</span>
                <h3 class="page-title">Add Syllabus</h3>
            </div>
            <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Syllabus", "action" => "index"]); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Syllabus</li>
                </ol>
            </div>
        </div>
        <!-- End Page Header -->
    </section>


    <section class="content">
        <div class="row">
            <div class="col-lg-7 col-sm-12 col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-body p-0 pb-3">
                        <div class="card-body d-flex flex-column">
                            <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>


                            <div class="form-group">
                                <label for="department_name">Department</label>
                                <select class="form-control" name="department_id" required>
                                    <option data-display="Select Department" value="" disabled selected>Please Select Department</option>
                                    <?php foreach ($departments as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['dept_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="sem_name">Semester</label>
                                <select class="form-control" name="semester_id" required>
                                    <option data-display="Select Semester" value="" disabled selected>Please Select Semester</option>
                                    <?php foreach ($semester as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['sem_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" placeholder="Please Enter Title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" rows="5" name="description" placeholder="Please Enter Description" required></textarea>
                                
                            </div>
                           
                                                   

                            <div class="form-group">
                                <label for="is_active">Is Active</label>
                                <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="form-group mb-0 float-right">
                                <button type="submit" class="btn btn-accent sticky-action-btn">Add Syllabus</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>