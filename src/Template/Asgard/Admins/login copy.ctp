<div class="p-5">
  <div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
  </div>
  <!-- <form class="user"> -->
  <?= $this->Form->create('Admin', ['class' => 'user']); ?>
    <div class="form-group">
      <input type="text" name="username" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
    </div>
    <div class="form-group">
      <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
    </div>
    <div class="form-group">
      <div class="custom-control custom-checkbox small">
        <input type="checkbox" class="custom-control-input" id="customCheck">
        <label class="custom-control-label" for="customCheck">Remember Me</label>
      </div>
    </div>
    <!-- <a href="<?= $this->Url->build(["controller" => "admins", "action" => "login"]);?>" class="btn btn-primary btn-user btn-block">
      Login
    </a>    -->
    <button type="submit" class="btn btn-primary btn-user btn-block">Sign In</button>
    <br>
    <a href="<?= $this->Url->build(["controller" => "admins", "action" => "forgot_password"]);?>">Forgot Password?</a>
  </form>
</div>

