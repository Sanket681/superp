<!-- Page Header -->
<section class="content-header">
  <div class="page-header row no-gutters py-4">
    <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
      <span class="text-uppercase page-subtitle">Asgard</span>
      <h3 class="page-title">Add Admin</h3>
    </div>
    <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Admins", "action" => "index"]); ?>">Home</a></li>
        <li class="breadcrumb-item active">Admins</li>
      </ol>
    </div>
  </div>
</section>
<!-- End Page Header -->

<section class="content">
  <div class="row">
    <div class="col-lg-7 col-sm-12 col-md-12">
      <div class="card card-small mb-4">
        <div class="card-body p-0 pb-3">
          <div class="card-body d-flex flex-column">
            <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Please Enter Your Fullname">
            </div>
            <div class="form-group">
              <label for="username">Username / Email</label>
              <input type="text" class="form-control" id="username" placeholder="Enter UserName" name="username" required>
            </div>

            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" required>
            </div>

            <div class="form-group">
              <label for="rpassword">Retype Password</label>
              <input type="password" class="form-control" id="rpassword" placeholder="Retype Password" name="rpassword" required>
            </div>

            <div class="file-upload">
              <button class="btn btn-accent btn-block" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>
              <div class="image-upload-wrap">
                <input class="file-upload-input" type='file' name="image_value" onchange="readURL(this);" accept="image/*" />
                <div class="drag-text">
                  <h5>Drag and drop a file or select add Image</h5>
                </div>
              </div>
              <div class="file-upload-content">
                <img id="product-primary-image" class="file-upload-image" src="#" alt="your image" />
                <div class="image-title-wrap">
                  <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="role">Role</label>
              <select class="form-control" name="role" id="role">
                <option value="SUPER_ADMIN">Super Admin</option>
                <option value="ADMIN">ADMIN</option>
                <option value="TEACHER">TEACHER</option>
                <option value="STUDENT">STUDENT</option>
              </select>
            </div>



            <div class="form-group mb-0 float-right">
              <button type="submit" class="btn btn-accent sticky-action-btn">Add Admin</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>