<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">View Issue</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Issue", "action" => "index"]); ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Issue</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section>
    <div class="row">
        <div class="col-lg-8 mx-auto mt-4">
            <div class="card card-small edit-user-details mb-4">
                <div class="card-header p-0">
                    <div class="edit-user-details__bg">
                        <img src="<?= $this->request->getAttribute('webroot') . 'img/backend/' ?>background.jpg" alt="Issue Details Background Image">
                        <label class="edit-user-details__change-background">
                            <i class="material-icons mr-1">receipt_long</i> Issue Details
                        </label>
                    </div>
                </div>
                <div class="card-body p-0">
                    <form action="#" class="py-4">
                        <div class="form-row mx-4">
                            <div class="<?php if (!empty($data['book']['image_dir']) || !empty($data['book']['image_value'])):?>col-lg-8<?php else:?>col-lg-12<?php endif;?>">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Book Name</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['book']['name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Book Author</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['book']['author'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Student Name</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['student']['first_name']?> <?= $data['student']['last_name']?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Teacher Name</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['teacher']['first_name']?> <?= $data['teacher']['last_name']?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Issue Date</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['issue_date']?> </p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Due Date</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['due_date']?> </p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Return Date</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['return_date']?> </p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Note</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['note']?> </p>
                                    </div>
                                   
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Is Active</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['is_active'] ? 'Yes' : 'No' ?></p>
                                    </div>
                                </div>
                               
                            </div>
                            <?php if(!empty($data['book']['image_dir']) || !empty($data['book']['image_value'])):?>
                            <div class="col-lg-4">
                                <h6 class="form-text m-0 text-center w-100 mb-4 "><b>Book Image</b></h6>

                                <div class="d-flex justify-content-center ">
                                    <img class="user-avatar rounded-circle mr-2" src="<?php if (!empty($data['book']['image_value'])) : ?><?= $this->request->getAttribute('webroot') . $data['book']['image_dir'] . $data['book']  ['image_value'] ?><?php else : ?><?= $this->request->getAttribute('webroot') ?>img/backend/1.jpg<?php endif; ?>" height="100" width="100" alt="User Avatar">
                                </div>
                                
                            </div>
                            <?php endif;?>

                            <div class="form-group mb-0">
                                <a href="<?= $this->Url->build(["controller" => "Issue", "action" => "edit", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-white active-light btn-sm sticky-action-btn-edit" data-toggle="tooltip" title="Edit Issue">
                                    <i class="material-icons"></i> Edit
                                </a>
                                <a href="<?= $this->Url->build(["controller" => "Issue", "action" => "delete", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-sm btn-danger sticky-action-btn-delete" data-toggle="tooltip" title="Delete Issue">
                                    <i class="material-icons white"></i> Delete
                                </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>