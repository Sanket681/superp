<div id="issue">
    <!-- Page Header -->
    <section class="content-header">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">SUPERP</span>
                <h3 class="page-title">Add Issue</h3>
            </div>
            <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Issue", "action" => "index"]); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Issue</li>
                </ol>
            </div>
        </div>
        <!-- End Page Header -->
    </section>

    <section class="content">
        <div class="row">
            <div class="col-lg-7 col-sm-12 col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-body p-0 pb-3">
                        <div class="card-body d-flex flex-column">
                            <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>

                            <div class="form-group">
                                <label for="book_name">Book</label>
                                <select class="form-control" name="book_id" required>
                                    <option data-display="Select Book" value="" disabled selected>Please Select Book</option>
                                    <?php foreach ($books as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="student_name">Student</label>
                                <select class="form-control" name="student_id">
                                    <option data-display="Select student" value="" disabled selected>Please Select Student</option>
                                    <?php foreach ($students as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['first_name']) ?> <?= ucwords($val['last_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="teacher_name">Teacher</label>
                                <select class="form-control" name="teacher_id">
                                    <option data-display="Select teacher" value="" disabled selected>Please Select Teacher</option>
                                    <?php foreach ($teachers as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['first_name']) ?> <?= ucwords($val['last_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="due_date">Due Date</label>
                                <input type="date" name="due_date" placeholder="Please Enter Due Date" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="note">Note</label>
                                <input type="text" name="note" placeholder="Please Enter Note" class="form-control">
                            </div>




                            <div class="form-group">
                                <label for="is_active">Is Active</label>
                                <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="form-group mb-0 float-right">
                                <button type="submit" class="btn btn-accent sticky-action-btn">Add Issue</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

