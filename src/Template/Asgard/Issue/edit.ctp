<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Issue</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Issue", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Issue</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>

                        <div class="form-group">
                            <label for="book_name">Book</label>
                            <select class="form-control" name="book_id" value="<?= $data['book_id'] ?>" required>
                                <option data-display="Select Book" value="" disabled selected>Please Select Book</option>
                                <?php foreach ($books as $key => $val) : ?>
                                    <option value="<?= $val['id'] ?>" <?= $data['book_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="student_name">Student</label>
                            <select class="form-control" name="student_id" value="<?= $data['student_id'] ?>" required>
                                <option data-display="Select Student" value="" disabled selected>Please Select student</option>
                                <?php foreach ($students as $key => $val) : ?>
                                    <option value="<?= $val['id'] ?>" <?= $data['student_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['first_name']) ?> <?= ucwords($val['last_name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="teacher_name">Teacher</label>
                            <select class="form-control" name="teacher_id" value="<?= $data['teacher_id'] ?>" required>
                                <option data-display="Select Teacher" value="" disabled selected>Please Select teacher</option>
                                <?php foreach ($teachers as $key => $val) : ?>
                                    <option value="<?= $val['id'] ?>" <?= $data['teacher_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['first_name']) ?> <?= ucwords($val['last_name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>


                       

                        <div class="form-group">
                            <label for="due_date">Due Date</label>
                            <input type="date" name="due_date" value="<?= $data['due_date'] ?>" placeholder="Please Enter Due Date" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="return_date">Return Date</label>
                            <input type="date" name="return_date" value="<?= $data['return_date'] ?>" placeholder="Please Enter Due Date" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <input type="text" name="note" value="<?= $data['note'] ?>" placeholder="Please Enter Note" class="form-control">
                        </div>



                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value="1" <?= $data['is_active'] == 1 ? 'selected' : ' ' ?>>Yes</option>
                                <option value="0" <?= $data['is_active'] == 0 ? 'selected' : ' ' ?>>No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Issue</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>