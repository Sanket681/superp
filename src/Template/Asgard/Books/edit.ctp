<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Book</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Books", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Books</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>


<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" value="<?= $data['name'] ?>" placeholder="Please Enter Book Name" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="author">Author</label>
                                    <input type="text" name="author" value="<?= $data['author'] ?>" placeholder="Please Enter Author Name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="price">Price</label>
                                    <input type="number" name="price" value="<?= $data['price'] ?>" placeholder="Please Enter Book Price" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="quantity">Quantity</label>
                                    <input type="number" name="quantity" value="<?= $data['quantity'] ?>" placeholder="Please Enter Quantity " class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="subject_code">Code</label>
                                    <input type="text" name="subject_code" value="<?= $data['subject_code'] ?>" placeholder="Please Enter Code" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="Available" <?= $data['status'] == 'AVAILABLE' ? 'selected' : ''  ?>>Available</option>
                                        <option value="Not Available" <?= $data['status'] ==   'NOT AVAILABLE' ? 'selected' : ''  ?>>Not Available</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="rack_no">Rack Number</label>
                            <input type="text" name="rack_no" value="<?= $data['rack_no'] ?>" placeholder="Please Enter Rack Number" class="form-control" required>
                        </div>

                        <div class="file-upload">
                            <button class="btn btn-accent btn-block" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Edit Image</button>
                            <div class="image-upload-wrap">
                                <input class="file-upload-input" type='file' name="image_value" onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h5>Drag and drop a file or select add Image</h5>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img id="product-primary-image" class="file-upload-image" src="<?= $this->request->getAttribute('webroot') . $data['image_dir'] . $data['image_value'] ?>" alt="your image" data="<?= $data['image_value'] ?>" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active">
                                <option value="1" <?= $data['is_active'] == 1 ? 'selected' : ' ' ?>>Yes</option>
                                <option value="0" <?= $data['is_active'] == 0 ? 'selected' : ' ' ?>>No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Book</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>