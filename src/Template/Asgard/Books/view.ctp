<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">View Book</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Books", "action" => "index"]); ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Books</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section>
    <div class="row">
        <div class="col-lg-8 mx-auto mt-4">
            <div class="card card-small edit-user-details mb-4">
                <div class="card-header p-0">
                    <div class="edit-user-details__bg">
                        <img src="<?= $this->request->getAttribute('webroot') . 'img/backend/' ?>background.jpg" alt="Book Details Background Image">
                        <label class="edit-user-details__change-background">
                            <i class="material-icons mr-1">receipt_long</i> Book Details
                        </label>
                    </div>
                </div>
                <div class="card-body p-0">
                    <form action="#" class="py-4">
                        <div class="form-row mx-4">
                            <div class="<?php if (!empty($data['image_dir']) || !empty($data['image_value'])) : ?>col-lg-8<?php else : ?>col-lg-12<?php endif; ?>">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Name</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Author</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['author'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Code</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['subject_code'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Price</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['price'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Quantity</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['quantity'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Rack Number</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['rack_no'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Status</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['status'] ?></p>
                                    </div>
                                   
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Is Active</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['is_active'] ? 'Yes' : 'No' ?></p>
                                    </div>
                                </div>

                            </div>
                            <?php if (!empty($data['image_dir']) || !empty($data['image_value'])) : ?>
                                <div class="col-lg-4">
                                    <h6 class="form-text m-0 text-center w-100 mb-4 "><b>Book Image</b></h6>

                                    <div class="d-flex justify-content-center ">
                                        <img class="user-avatar rounded-circle mr-2" src="<?php if (!empty($data['image_value'])) : ?><?= $this->request->getAttribute('webroot') . $data['image_dir'] . $data['image_value'] ?><?php else : ?><?= $this->request->getAttribute('webroot') ?>img/backend/1.jpg<?php endif; ?>" height="100" width="100" alt="User Avatar">
                                    </div>

                                </div>
                            <?php endif; ?>

                            <div class="form-group mb-0">
                                <a href="<?= $this->Url->build(["controller" => "Books", "action" => "edit", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-white active-light btn-sm sticky-action-btn-edit" data-toggle="tooltip" title="Edit Book">
                                    <i class="material-icons"></i> Edit
                                </a>
                                <a href="<?= $this->Url->build(["controller" => "Books", "action" => "delete", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-sm btn-danger sticky-action-btn-delete" data-toggle="tooltip" title="Delete Book">
                                    <i class="material-icons white"></i> Delete
                                </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>