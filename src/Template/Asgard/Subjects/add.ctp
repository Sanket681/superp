    <!-- Page Header -->
    <section class="content-header">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">SUPERP</span>
                <h3 class="page-title">Add Subjects</h3>
            </div>
            <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Subjects", "action" => "index"]); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Subjects</li>
                </ol>
            </div>
        </div>
        <!-- End Page Header -->
    </section>


    <section class="content">
        <div class="row">
            <div class="col-lg-7 col-sm-12 col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-body p-0 pb-3">
                        <div class="card-body d-flex flex-column">
                            <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>


                            <div class="form-group">
                                <label for="department_name">Department</label>
                                <select class="form-control" name="department_id" required>
                                    <option data-display="Select Department" value="" disabled selected>Please Select Department</option>
                                    <?php foreach ($departments as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['dept_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="teacher_name">Teacher</label>
                                <select class="form-control" name="teacher_id">
                                    <option data-display="Select teacher" value="" disabled selected>Please Select Teacher</option>
                                    <?php foreach ($teachers as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['first_name']) ?> <?= ucwords($val['last_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="sem_name">Semester</label>
                                <select class="form-control" name="semester_id" required>
                                    <option data-display="Select Semester" value="" disabled selected>Please Select Semester</option>
                                    <?php foreach ($semester as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>"><?= ucwords($val['sem_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="subject_name">Subject Name</label>
                                <input type="text" name="subject_name" placeholder="Please Enter Subject Name" class="form-control" required>
                            </div>
                           
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="subject_code">Subject Code</label>
                                        <input type="text" name="subject_code" placeholder="Please Enter Subject Code" class="form-control" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="subject_type">Subject Type</label>
                                        <select class="form-control" name="subject_type" id="subject_type">
                                            <option value="MANDATORY">Mandatory</option>
                                            <option value="OPTIONAL">Optional</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="pass_mark">Pass Mark</label>
                                        <input type="number" name="pass_mark" placeholder="Please Enter Pass Mark" class="form-control" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="final_mark">Final Mark</label>
                                        <input type="number" name="final_mark" placeholder="Please Enter Final Mark" class="form-control" required>

                                    </div>
                                </div>
                            </div>
                           

                            <div class="form-group">
                                <label for="is_active">Is Active</label>
                                <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="form-group mb-0 float-right">
                                <button type="submit" class="btn btn-accent sticky-action-btn">Add Subject</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>