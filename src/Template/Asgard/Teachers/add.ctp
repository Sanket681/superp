<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Add Teacher</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Teachers", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Teachers</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>

                        <div class="form-group">
                            <label for="department_name">Department</label>
                            <select class="form-control" name="department_id" required>
                                <option data-display="Select Department" value="" disabled selected>Please Select Department</option>
                                <?php foreach ($departments as $key => $val) : ?>
                                    <option value="<?= $val['id'] ?>"><?= ucwords($val['dept_name']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="name">First Name</label>
                                    <input type="text" name="first_name" placeholder="Please Enter First Name" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="name">Last Name</label>
                                    <input type="text" name="last_name" placeholder="Please Enter Last Name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="dob">Date of Birth</label>
                                    <input type="date" name="dob" placeholder="Please Enter Dob" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="gender">Gender</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" placeholder="Please Enter Email" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="mobile">Mobile</label>
                                    <input type="text" name="mobile" pattern="[0-9]{10}" title="Enter Valid Mobile Number" placeholder="Please Enter Mobile Number" class="form-control" required>
                                </div>
                                <div class="col-sm-6">
                                    <label for="blood_group">Blood Group</label>
                                    <select class="form-control" name="blood_group">
                                        <option data-display="Select Blood Group" value="" disabled selected>Please Select Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="file-upload">
                            <button class="btn btn-accent btn-block" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>
                            <div class="image-upload-wrap">
                                <input class="file-upload-input" type='file' name="image_value" onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h5>Drag and drop a file or select add Image</h5>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img id="product-primary-image" class="file-upload-image" src="#" alt="your image" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <textarea name="address" placeholder="Please Enter Address" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="qualification">Qualification</label>
                            <input type="text" name="qualification" placeholder="Please Enter Qualification" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Add Teacher</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>