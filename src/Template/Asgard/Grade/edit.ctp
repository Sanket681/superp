<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Grade</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Grade", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Grade</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>
                                             
                                             
                        <div class="form-group">
                            <label for="grade_name">Grade Name</label>
                            <input type="text" name="grade_name" value="<?= $data['grade_name']?>" placeholder="Please Enter Grade Name" class="form-control" required>                       
                        </div>
                        <div class="form-group">
                            <label for="grade_point">Grade Point</label>
                            <input type="number" name="grade_point" value="<?= $data['grade_point']?>" placeholder="Please Enter Grade Point" class="form-control" required>                       
                        </div>
                      
                        <div class="form-group">
                            <label for="mark_from">Mark From</label>
                            <input type="number" name="mark_from" value="<?= $data['mark_from']?>" placeholder="Please Enter Mark From" class="form-control" required>                       
                        </div>
                      
                        <div class="form-group">
                            <label for="mark_upto">Mark Upto</label>
                            <input type="number" name="mark_upto" value="<?= $data['mark_upto']?>" placeholder="Please Enter Mark Upto" class="form-control" required>                       
                        </div>
                      
                       
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" placeholder="Please Enter Note" class="form-control" required><?= $data['note']?></textarea>
                                                
                        </div>

                        <div class="file-upload">
                            <button class="btn btn-accent btn-block" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Edit Image</button>
                            <div class="image-upload-wrap">
                                <input class="file-upload-input" type='file' name="image_value" onchange="readURL(this);" accept="image/*" />
                                <div class="drag-text">
                                    <h5>Drag and drop a file or select add Image</h5>
                                </div>
                            </div>
                            <div class="file-upload-content">
                                <img id="product-primary-image" class="file-upload-image" src="<?= $this->request->getAttribute('webroot') . $data['image_dir'] . $data['image_value'] ?>" alt="your image" data="<?= $data['image_value'] ?>" />
                                <div class="image-title-wrap">
                                    <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                </div>
                            </div>
                        </div>

                       
                       

                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active"  @change="onTypeChange">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Grade</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>