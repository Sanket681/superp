    <!-- Page Header -->
    <section class="content-header">
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">SUPERP</span>
                <h3 class="page-title">Edit Exam Schedule</h3>
            </div>
            <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "index"]); ?>">Home</a></li>
                    <li class="breadcrumb-item active">Exam Schedule</li>
                </ol>
            </div>
        </div>
        <!-- End Page Header -->
    </section>


    <section class="content">
        <div class="row">
            <div class="col-lg-7 col-sm-12 col-md-12">
                <div class="card card-small mb-4">
                    <div class="card-body p-0 pb-3">
                        <div class="card-body d-flex flex-column">
                            <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>


                            <div class="form-group">
                                <label for="department_name">Department</label>
                                <select class="form-control" name="department_id" value="<?= $data['department_id'] ?>" required>
                                    <option data-display="Select Department" value="" disabled selected>Please Select Department</option>
                                    <?php foreach ($departments as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>" <?= $data['department_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['dept_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="exam_name">Exam Name</label>
                                <select class="form-control" name="exam_id" value="<?= $data['exam_id'] ?>" required>
                                    <option data-display="Select Exam" value="" disabled selected>Please Select Exam Name</option>
                                    <?php foreach ($exam as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>" <?= $data['exam_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['exam_name']) ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="subject_name">Subject</label>
                                <select class="form-control" name="subject_id" value="<?= $data['subject_id'] ?>" required>
                                    <option data-display="Select Subject" value="" disabled selected>Please Select Subject</option>
                                    <?php foreach ($subjects as $key => $val) : ?>
                                        <option value="<?= $val['id'] ?>" <?= $data['subject_id'] == $val['id'] ? 'selected' : '' ?>><?= ucwords($val['subject_name']) ?> </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="date">Date</label>
                                        <input type="date" name="date" value="<?= $data['date'] ?>" placeholder="Please Enter Date" class="form-control" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="room">Room</label>
                                        <input type="text" name="room" value="<?= $data['room'] ?>" placeholder="Please Enter Room Number" class="form-control" required>

                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="time_from">Time From</label>
                                            <input type="time" name="time_from" value="<?= $data['time_from'] ?>" placeholder="Please Enter Time From" class="form-control" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="time_to">Time To</label>
                                            <input type="time" name="time_to" value="<?= $data['time_to'] ?>" placeholder="Please Enter Time To" class="form-control" required>
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="is_active">Is Active</label>
                                    <select class="form-control" name="is_active" id="is_active">
                                        <option value="1" <?= $data['is_active'] == 1 ? 'selected' : ' ' ?>>Yes</option>
                                        <option value="0" <?= $data['is_active'] == 0 ? 'selected' : ' ' ?>>No</option>
                                    </select>
                                </div>
                                <div class="form-group mb-0 float-right">
                                    <button type="submit" class="btn btn-accent sticky-action-btn">Edit Exam Schedule</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>