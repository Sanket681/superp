    <div id="schedule-view">

        <!-- Page Header -->
        <section class="content-header">
            <div class="page-header row no-gutters py-4">
                <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                    <span class="text-uppercase page-subtitle">SUPERP</span>
                    <h3 class="page-title">Exam Schedule</h3>
                </div>
                <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "index"]); ?>">Home</a></li>
                        <li class="breadcrumb-item active">Exam Schedule</li>
                    </ol>
                </div>
            </div>
            <!-- End Page Header -->
        </section>

        <section class="content">
            <div class="row">
                <div class="col">
                    <div class="card card-small mb-4">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Active Records</h6>
                        </div>
                        <div class="p-0 text-center">
                            <div class="card-body d-flex flex-column">
                                <?= $this->Form->create(null, ['type' => 'get', 'class' => 'quick-post-form']); ?>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" name="filter" id="filter" @change="onFilterChange">
                                                <option data-display="Filter" value="">Select Filter</option>
                                                <option value="department_id" <?= $filter == 'department_id' ? 'selected' : '' ?>>Department</option>
                                                <option value="date" <?= $filter == 'date' ? 'selected' : '' ?>>
                                                    Date</option>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-sm-6 col-md-6 d-flex" v-if="department">
                                        <div class="form-group">
                                            <select class="form-control" name="department_id" required>
                                                <option data-display="Select Departments" value="" disabled selected>Please Select Departments</option>
                                                <?php foreach ($departments as $k => $v) : ?>
                                                    <option value="<?= $v['id'] ?>" <?php if (isset($department_selected)) : ?><?= $department_selected == $v['id'] ? 'selected' : '' ?><?php endif; ?>><?= $v['dept_name'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>


                                        <div class="form-group pl-3">
                                            <button type="submit" class="btn btn-accent" data-toggle="tooltip" title="Submit">Search</button>
                                        </div>

                                    </div>


                                    <div class="col-lg-3 col-sm-6 col-md-6 d-flex" v-if="search">
                                        <div class="form-group w-100">
                                            <input type="text" class="form-control" name="search" title="What would you like to search" value="<?= $search ?>" placeholder="Search Here...">

                                        </div>
                                        <div class="form-group pl-3">
                                            <button type="submit" class="btn btn-accent" data-toggle="tooltip" title="Submit">Search</button>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-sm-6 col-md-6 d-flex justify-content-end align-items-center">
                                        <a class="btn btn-accent fab-btn" href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "add"]); ?>" data-toggle="tooltip" title="Add Examschedule">
                                            <i class="material-icons">add</i>
                                            <span>Add Exam Schedule</span>
                                        </a>
                                    </div>
                                </div>
                                </form>

                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead class="bg-light">
                                            <tr>

                                                <th scope="col" class="border-0">Exam Name</th>
                                                <th scope="col" class="border-0">Department</th>
                                                <th scope="col" class="border-0">Subject</th>
                                                <th scope="col" class="border-0">Date</th>
                                                <th scope="col" class="border-0">Time</th>
                                                <th scope="col" class="border-0">Room</th>
                                                <th scope="col" class="border-0">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data as $key => $val) : ?>
                                                <tr>

                                                    <td><?= $val['exam']['exam_name'] ?></td>
                                                    <td><?= $val['departments']['dept_name'] ?></td>
                                                    <td><?= $val['subjects']['subject_name'] ?></td>
                                                    <td><?= $val['date'] ?></td>
                                                    <td><?= $val['time_from'] ?> - <?= $val['time_to'] ?></td>
                                                    <td><?= $val['room'] ?></td>


                                                    <td class="action-btn-container">
                                                        <div class="btn-group btn-group-sm " role="group" aria-label="Table row actions">
                                                            <a href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "view", $val['id']]); ?>" type="button" class="btn btn-white active-light" data-toggle="tooltip" title="View Examschedule">
                                                                <i class="material-icons white">visibility</i>
                                                            </a>
                                                            <a href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "edit", $val['id']]); ?>" type="button" class="btn btn-white active-light" data-toggle="tooltip" title="Edit Examschedule">
                                                                <i class="material-icons"></i>
                                                            </a>
                                                            <a href="<?= $this->Url->build(["controller" => "Examschedule", "action" => "delete", $val['id']]); ?>" type="button" class="btn btn-danger" data-toggle="tooltip" title="Delete Examschedule">
                                                                <i class="material-icons white"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer" style="background: #fbfbfb;">
                                <!-- Pagination -->
                                <div class="row d-flex justify-content-center align-items-center">
                                    <div class="col-sm-12 col-md-5 d-flex">
                                        <div class="">
                                            <?= $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries') ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="dataTables_paginate paging_simple_numbers d-flex justify-content-end">
                                            <ul class="pagination">
                                                <?php
                                                echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'route' => 1));
                                                echo $this->Paginator->next(__('Next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>



    <?php $this->start('extra-scripts'); ?>
    <script>
        new Vue({
            el: '#schedule-view',
            data() {
                return {
                    filter: 'department_id',
                    department: false,
                    search: true
                }
            },
            methods: {
                onFilterChange(e) {
                    const role = e.target.value;
                    if (role == 'department_id') {
                        this.department = true;
                        this.search = false;
                    } else {
                        this.department = false;
                        this.search = true;
                    }
                },
            },
            created() {
                console.log("Department Component Loaded.");
                let role = document.getElementById('filter').value;
                if (role == 'department_id') {
                    this.department = true;
                    this.search = false;
                } else {
                    this.department = false;
                    this.search = true;
                }
            }
        });
    </script>
    <?php $this->end(); ?>