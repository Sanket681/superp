<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Department</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Departments", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Departments</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>

                        <div class="form-group">
                            <label for="dept_name">Department Name</label>
                            <input type="text" name="dept_name" value="<?= $data['dept_name'] ?>" placeholder="Please Enter Department Name" class="form-control" required>

                        </div>

                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active" @change="onTypeChange">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Department</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>