<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">View Assignment</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Assignment", "action" => "index"]); ?>">Home</a>
                </li>
                <li class="breadcrumb-item active">Assignment</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section>
    <div class="row">
        <div class="col-lg-8 mx-auto mt-4">
            <div class="card card-small edit-user-details mb-4">
                <div class="card-header p-0">
                    <div class="edit-user-details__bg">
                        <img src="<?= $this->request->getAttribute('webroot') . 'img/backend/' ?>background.jpg" alt="Assignment Details Background Image">
                        <label class="edit-user-details__change-background">
                            <i class="material-icons mr-1">receipt_long</i> Assignment Details
                        </label>
                    </div>
                </div>
                <div class="card-body p-0">
                    <form action="#" class="py-4">
                        <div class="form-row mx-4">
                            <div class="col-lg-12">
                                <div class="form-row">
                                 
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Department</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['department']['dept_name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Semester</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['semester']['sem_name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Subject</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['subject']['subject_name'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Title</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['title'] ?></p>
                                    </div>
                                   
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Deadline</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['deadline'] ?></p>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Upload At</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['created_at'] ?></p>
                                    </div>
                                   
                                    
                                    <div class="form-group col-md-6">
                                        <h6 class="form-text m-0"><b>Is Active</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['is_active'] ? 'Yes' : 'No' ?></p>
                                    </div>
                                    <div class="form-group col-md-12 text-justify">
                                        <h6 class="form-text m-0"><b>Description</b></h6>
                                        <p class="form-text text-muted m-0"><?= $data['description'] ?></p>
                                    </div>
                                </div>
                               
                            </div>
                           

                            <div class="form-group mb-0">
                                <a href="<?= $this->Url->build(["controller" => "Assignment", "action" => "edit", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-white active-light btn-sm sticky-action-btn-edit" data-toggle="tooltip" title="Edit Assignment">
                                    <i class="material-icons"></i> Edit
                                </a>
                                <a href="<?= $this->Url->build(["controller" => "Assignment", "action" => "delete", $data['id']]); ?>" style="font-size: 14px;" type="button" class="btn btn-sm btn-danger sticky-action-btn-delete" data-toggle="tooltip" title="Delete Assignment">
                                    <i class="material-icons white"></i> Delete
                                </a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>