<!-- Page Header -->
<section class="content-header">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
            <span class="text-uppercase page-subtitle">SUPERP</span>
            <h3 class="page-title">Edit Transport</h3>
        </div>
        <div class="d-none d-sm-block offset-sm-4 col-4 col-12 col-sm-4 justify-content-end">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(["controller" => "Transport", "action" => "index"]); ?>">Home</a></li>
                <li class="breadcrumb-item active">Transport</li>
            </ol>
        </div>
    </div>
    <!-- End Page Header -->
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-7 col-sm-12 col-md-12">
            <div class="card card-small mb-4">
                <div class="card-body p-0 pb-3">
                    <div class="card-body d-flex flex-column">
                        <?= $this->Form->create(null, ['type' => 'file', 'role' => 'form']); ?>
                                             
                                             
                        <div class="form-group">
                            <label for="route_name">Route Name</label>
                            <input type="text" name="route_name" value="<?= $data['route_name'] ?>" placeholder="Please Enter Route Name" class="form-control" required>                       
                        </div>
                        <div class="form-group">
                            <label for="number_of_vehicle">Number of Vehicle</label>
                            <input type="number" name="number_of_vehicle" value="<?= $data['number_of_vehicle'] ?>"  placeholder="Please Enter Number of Vehicle" class="form-control" required>                       
                        </div>
                        <div class="form-group">
                            <label for="route_fare">Route Fare</label>
                            <input type="number" name="route_fare" value="<?= $data['route_fare'] ?>"  placeholder="Please Enter Route Fare" class="form-control" required>                       
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" placeholder="Please Enter Note" class="form-control"><?= $data['note'] ?></textarea>
                                                
                        </div>
                       

                        <div class="form-group">
                            <label for="is_active">Is Active</label>
                            <select class="form-control" name="is_active" id="is_active"  @change="onTypeChange">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group mb-0 float-right">
                            <button type="submit" class="btn btn-accent sticky-action-btn">Edit Transport</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>